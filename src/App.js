import "./App.css";
import Profile from "./components/Profile";
import Body from "./components/Body";
import Footer from "./components/Footer";

function App() {
    return (
        <div className="app-container">
            <Profile />
            <Body />
            <Footer />
        </div>
    );
}

export default App;
