import React from "react";
import profileImage from "../assets/Profile.svg";

export default function Profile() {
    return (
        <img
            src={profileImage}
            alt="Owner of card"
            style={{ width: "317px" }}
        />
    );
}
