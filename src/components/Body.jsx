import React from "react";
import "./Body.css";

import Info from "./body-components/Info";
import BodySection from "./body-components/BodySection";

export default function Body() {
    let aboutProps = {
        header: "About",
        para: "I am a frontend developer with a particular interest in making things simple and automating daily tasks. I try to keep up with security and best practices, and am always looking for new things to learn.",
    };

    let interestsProps = {
        header: "Interests",
        para: "Food expert. Music scholar. Reader. Internet fanatic. Bacon buff. Entrepreneur. Travel geek. Pop culture ninja. Coffee fanatic.",
    };

    return (
        <div className="container">
            <Info />
            <div className="body-sections">
                <BodySection {...aboutProps} />
                <BodySection {...interestsProps} />
            </div>
        </div>
    );
}
