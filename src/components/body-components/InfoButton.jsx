import React from "react";
import "./InfoButton.css";

export default function InfoButton(props) {
    return (
        <button
            type="button"
            style={{
                background: props.backgroundColor,
                color: props.textColor,
                border: "1px solid " + props.backgroundColor,
            }}
        >
            <img src={props.iconPath} alt="Icon" />
            {props.text}
        </button>
    );
}
