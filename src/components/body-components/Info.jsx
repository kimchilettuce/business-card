import React from "react";
import "./Info.css";
import InfoButton from "./InfoButton";

import iconEmail from "../../assets/Mail.svg";
import iconLinkedIn from "../../assets/linkedin.svg";

export default function Info() {
    let emailProps = {
        iconPath: iconEmail,
        backgroundColor: "#FFFFFF",
        text: "Email",
        textColor: "#374151",
    };

    let linkedInProps = {
        iconPath: iconLinkedIn,
        backgroundColor: "#5093E2",
        text: "LinkedIn",
        textColor: "#FFFFFF",
    };

    return (
        <>
            <h1 className="info-name">Laura Smith </h1>
            <h1 className="info-description">Frontend Developer</h1>
            <h3 className="info-website">laurasmith.website</h3>
            <div className="info-social-buttons">
                <InfoButton {...emailProps} />
                <InfoButton {...linkedInProps} />
            </div>
        </>
    );
}
