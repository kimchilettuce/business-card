import React from "react";
import "./BodySection.css";

export default function BodySection(props) {
    return (
        <div className="body-container">
            <h1 className="body-header">{props.header}</h1>
            <h3 className="body-para">
                {props.para}
            </h3>
        </div>
    );
}
