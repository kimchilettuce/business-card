import React from "react";
import "./Footer.css";

import twitterIcon from "../assets/Twitter Icon.svg";
import fbIcon from "../assets/Facebook Icon.svg";
import instIcon from "../assets/Instagram Icon.svg";
import gitHubIcon from "../assets/GitHub Icon.svg";

export default function Footer() {
    return (
        <div className="footer-container">
            <img src={twitterIcon} alt="Twitter" />
            <img src={fbIcon} alt="Facebook" />
            <img src={instIcon} alt="Instagram" />
            <img src={gitHubIcon} alt="Github" />
        </div>
    );
}
